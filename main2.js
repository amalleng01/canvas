var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.lineWidth = 2;
//1
ctx.beginPath();
ctx.moveTo(60, 0);
ctx.lineTo(0,0);
ctx.fillStyle = 'orange';
ctx.fillRect(0,0, 60, 30);
ctx.stroke();


ctx.beginPath();
ctx.moveTo(60, 0);
ctx.lineTo(60,30);
ctx.fillStyle = 'red';
ctx.fillRect(60,0, 60, 60);
ctx.fillStyle = 'red';
ctx.fillRect(0,30, 64, 30);
ctx.stroke();


ctx.beginPath();
ctx.moveTo(60, 30);
ctx.lineTo(0,30);
ctx.fillStyle = 'blue';
ctx.fillRect(120,0, 60, 60);
ctx.fillStyle = 'blue';
ctx.fillRect(0,60, 180, 30);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(0, 30);
ctx.lineTo(0,0);
ctx.fillStyle = 'green';
ctx.fillRect(180,0, 60, 90);
ctx.fillStyle = 'green';
ctx.fillRect(0,90, 240, 30);
ctx.stroke();

//2
ctx.beginPath();
ctx.moveTo(120, 0);
ctx.lineTo(0,0);
ctx.fillStyle = 'yellow';
ctx.fillRect(240,0, 60, 120);
ctx.fillStyle = 'yellow';
ctx.fillRect(0,120, 300, 30);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(120, 0);
ctx.lineTo(120,60);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(120, 60);
ctx.lineTo(0,60);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(0, 60);
ctx.lineTo(0,0);
ctx.stroke();

//3
ctx.beginPath();
ctx.moveTo(180, 0);
ctx.lineTo(0,0);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(180, 0);
ctx.lineTo(180,90);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(180, 90);
ctx.lineTo(0,90);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(0, 90);
ctx.lineTo(0,0);
ctx.stroke();

//4
ctx.beginPath();
ctx.moveTo(240, 0);
ctx.lineTo(0,0);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(240, 0);
ctx.lineTo(240,120);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(240, 120);
ctx.lineTo(0,120);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(0, 120);
ctx.lineTo(0,0);
ctx.stroke();

//5
ctx.beginPath();
ctx.moveTo(300, 0);
ctx.lineTo(0,0);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(300, 0);
ctx.lineTo(300,150);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(300, 150);
ctx.lineTo(0,150);
ctx.stroke();

ctx.beginPath();
ctx.moveTo(0, 150);
ctx.lineTo(0,0);
ctx.stroke();
